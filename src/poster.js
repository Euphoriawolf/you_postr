function createResource(properties) {
  var resource = {};
  var normalizedProps = properties;
  for (var p in properties) {
    var value = properties[p];
    if (p && p.substr(-2, 2) == "[]") {
      var adjustedName = p.replace("[]", "");
      if (value) {
        normalizedProps[adjustedName] = value.split(",");
      }
      delete normalizedProps[p];
    }
  }
  for (var prop in normalizedProps) {
    // Leave properties that don't have values out of inserted resource.
    if (normalizedProps.hasOwnProperty(prop) && normalizedProps[prop]) {
      var propArray = prop.split(".");
      var ref = resource;
      for (var pa = 0; pa < propArray.length; pa++) {
        var key = propArray[pa];
        if (pa == propArray.length - 1) {
          ref[key] = normalizedProps[prop];
        } else {
          ref = ref[key] = ref[key] || {};
        }
      }
    }
  }
  return resource;
}

function removeEmptyParams(params) {
  for (var p in params) {
    if (!params[p] || params[p] == "undefined") {
      delete params[p];
    }
  }
  return params;
}

const buildApiRequest = (requestMethod, path, params, properties) => {
  const gapi = window.gapi;
  params = removeEmptyParams(params);

  return new Promise((resolve, reject) => {
    if (properties) {
      var resource = createResource(properties);
      gapi.client
        .request({
          body: resource,
          method: requestMethod,
          path: path,
          params: params
        })
        .then(response => {
          resolve(response.result);
        })
        .catch(error => {
          reject(error.result);
        });
    } else {
      gapi.client
        .request({
          method: requestMethod,
          path: path,
          params: params
        })
        .then(response => {
          resolve(response.result);
        })
        .catch(error => {
          reject(error.result);
        });
    }
  });
};

export const postTopLevelComment = (videoId, comment) => {
  return new Promise((resolve, reject) => {
    buildApiRequest(
      "POST",
      "/youtube/v3/commentThreads",
      { part: "snippet" },
      {
        "snippet.videoId": videoId,
        "snippet.topLevelComment.snippet.textOriginal": comment
      }
    )
      .then(result => resolve(result))
      .catch(error => reject(error));
  });
};

export const postFollowUpComment = (parentCommentId, comment) => {
  return new Promise((resolve, reject) => {
    buildApiRequest(
      "POST",
      "/youtube/v3/comments",
      { part: "snippet" },
      {
        "snippet.parentId": parentCommentId,
        "snippet.textOriginal": comment
      }
    )
      .then(result => resolve(result))
      .catch(error => reject(error));
  });
};
