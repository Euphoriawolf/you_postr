import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import "./registerServiceWorker";

import Vuesax from "vuesax";
import "vuesax/dist/vuesax.css"; //Vuesax styles
import "material-icons/iconfont/material-icons.css";

import VueTour from "vue-tour";
import "vue-tour/dist/vue-tour.css";

Vue.config.productionTip = false;
Vue.use(Vuesax, {
  theme: {
    colors: {
      primary: "#5b3cc4",
      secondary: "rgb(23, 201, 100)",
      success: "rgb(23, 201, 100)",
      danger: "rgb(242, 19, 93)",
      warning: "rgb(255, 130, 0)",
      dark: "rgb(184, 186, 191)"
    }
  }
});
Vue.use(VueTour);

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
