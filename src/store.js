import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    signedIn: false,
    currentUser: null,
    GoogleAuth: null,
    networkError: null
  },

  getters: {
    googleAuth: state => {
      return state.GoogleAuth;
    },

    networkError: state => {
      return state.networkError;
    }
  },

  mutations: {
    UPDATE_SIGN_IN(state, payload) {
      state.signedIn = payload;
    },

    SET_CURRENT_USER(state, currentUser) {
      state.currentUser = currentUser;
    },

    SET_GOOGLE_AUTH(state, googleAuth) {
      state.GoogleAuth = googleAuth;
    },

    ANNOUNCE_NETWORK_ERROR(state, payload) {
      state.networkError = payload;
    }
  },

  actions: {
    setGoogleAuth({ commit }, googleAuth) {
      commit("SET_GOOGLE_AUTH", googleAuth);

      if (googleAuth) {
        commit("SET_CURRENT_USER", googleAuth.currentUser.get());
        commit("UPDATE_SIGN_IN", true);
      } else {
        commit("SET_CURRENT_USER", null);
        commit("UPDATE_SIGN_IN", false);
      }
    },

    announceNetworkError({ commit }, payload) {
      commit("ANNOUNCE_NETWORK_ERROR", payload);
    }
  }
});
