export default {
  methods: {
    notifyNoTopCommentError() {
      this.$vs.notify({
        title: "No Main Comment Posted",
        text: "Post a main comment",
        color: "secondary",
        position: "top-right"
      });
    },

    notifyPostingSuccess(authorDisplayName) {
      this.$vs.notify({
        title: "Comment Posted",
        text: `${authorDisplayName} has posted successful`,
        color: "success",
        position: "top-right"
      });
    },

    notifyPostingError(errorMessage) {
      this.$vs.notify({
        title: "Error Posting To Youtube; Connect Account To Youtube",
        text: errorMessage,
        color: "danger",
        position: "top-right"
      });
    },

    notifyCommentError() {
      this.$vs.notify({
        title: "Cannot Post Empty Comment",
        text: "Add a comment to post",
        color: "danger",
        position: "top-right"
      });
    },

    notifyNoYoutubeLinkError() {
      this.$vs.notify({
        title: "No link added",
        text: "Add youtube link",
        color: "danger",
        position: "top-right"
      });
    },

    notifyInvalidYoutubeLinkError() {
      this.$vs.notify({
        title: "Invalid Link",
        text: "Enter a valid youtube link",
        color: "danger",
        position: "top-right"
      });
    }
  }
};
