export default {
  data() {
    return {
      tourSteps: [
        {
          target: "#v-step-0",
          content: `Add Youtube links`
        },
        {
          target: "#v-step-1",
          content: `Link Ids appear here`
        },
        {
          target: "#v-step-2",
          content: `Enter main comment and post`
        },
        {
          target: "#v-step-3",
          content: `Enter follow up comments and post`
        },
        {
          target: "#v-step-4",
          content: `Enjoy your posting<br><br>Ensure to make suggestions, for features you would like to see in<br><strong>YouPostr</strong>`
        }
      ],
      tourCallbacks: {
        onStop: this.stopTour
      }
    };
  },

  mounted() {
    if (!localStorage.getItem("tourComplete")) {
      this.$tours["youPostrTour"].start();
    }
  },

  methods: {
    stopTour() {
      localStorage.setItem("tourComplete", true);
    }
  }
};
