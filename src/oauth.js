import store from "./store";

let GoogleAuth;
const gapi = window.gapi;

const scopes =
  "https://www.googleapis.com/auth/youtube.force-ssl https://www.googleapis.com/auth/youtube";
const discoveryDocs = [
  "https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest"
];

const apiKey = "AIzaSyBTvlxHwMrCC6jCWNumeA2JmStIp7A_iXk";
const clientId =
  "487496097174-3o1mjl0idqmn35mm65c3d7627s1bcper.apps.googleusercontent.com";

export const handleClientLoad = () => {
  // load the API client and auth2 library
  gapi.load("client:auth2", initClient);
};

const initClient = () => {
  console.log("%c Init function has run", "color: purple; font-weight: bolder");
  gapi.client
    .init({
      apiKey: apiKey,
      discoveryDocs: discoveryDocs,
      clientId: clientId,
      scope: scopes
    })
    .then(() => {
      GoogleAuth = gapi.auth2.getAuthInstance();

      let currentUser = GoogleAuth.currentUser.get();
      console.log(currentUser.hasGrantedScopes(scopes));

      // Listen for sign-in state changes.
      GoogleAuth.isSignedIn.listen(updateSigninStatus);

      // Handle the initial sign-in state.
      updateSigninStatus(GoogleAuth.isSignedIn.get());
    })
    .catch(error => {
      console.log(error);
      store.dispatch("announceNetworkError", {
        state: true,
        message: error.error.message
      });
    });
};

let updateSigninStatus = isSignedIn => {
  if (isSignedIn) {
    console.log(
      "%c The user is signed in",
      "color: green; font-weight: bolder;"
    );
    store.dispatch("setGoogleAuth", GoogleAuth);
  } else {
    console.log(
      "%c The user is not signed in",
      "color: red; font-weight: bolder"
    );
    store.dispatch("setGoogleAuth", null);
  }
};

export const authorizeUser = () => {
  return new Promise((resolve, reject) => {
    gapi.auth2
      .getAuthInstance()
      .signIn()
      .then(() => {
        resolve();
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const signOutUser = () => {
  gapi.auth2.getAuthInstance().signOut();
};
